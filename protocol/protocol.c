#include "protocol.h"

#include <assert.h>
#include <malloc.h>
#include <string.h>

#include "utils.h"

#define PACKET_TAG MKTAG('G', 'L', 'B', 'C')

static uint16 packet_calculate_length(Packet *packet) {
	assert(packet);

	uint16 length = PACKET_HEADER_SIZE;
	switch (packet->opcode) {
	case SMSG_HANDSHAKE:
	case CMSG_HANDSHAKE:
		length += sizeof(packet->handshake.id);
		break;
	case CMSG_LAUNCH_PROCESS:
		if (packet->cmsg_launch_process.name)
			length += (uint16)strlen(packet->cmsg_launch_process.name);
		length += sizeof(packet->cmsg_launch_process.name_len);
		break;
	case SMSG_APP_STARTED:
	case CMSG_TERMINATE_PROCESS:
	case SMSG_APP_TERMINATED:
	case SMSG_APP_TERMINATED_BY_OTHER:
	case SMSG_APP_ALIVE:
		length += sizeof(packet->smsg_app_alive.pid);
		break;
	default:
		break;
	}
	return length;
}

Packet *packet_create() {
	Packet *packet = (Packet *)calloc(1, sizeof(Packet));
	if (!packet)
		return NULL;

	packet->tag = PACKET_TAG;
	packet->length = PACKET_HEADER_SIZE;
	packet->version = PROTOCOL_VERSION;
	packet->opcode = MSG_NULL;

	return packet;
}

void packet_free(Packet *packet) {
	if (!packet)
		return;

	switch (packet->opcode) {
	case CMSG_LAUNCH_PROCESS:
		free(packet->cmsg_launch_process.name);
		break;
	default:
		break;
	}

	free(packet);
}

byte *packet_pack(Packet *packet) {
	if (!packet)
		return NULL;

	if (packet->version != PROTOCOL_VERSION)
		return NULL;

	if (packet->opcode >= MSG_INVALID)
		return NULL;

	packet->length = packet_calculate_length(packet);

	byte *packed_data = (byte *)malloc(packet->length);
	if (!packed_data)
		return NULL;

	byte *curr = packed_data;

	pack_uint32(&curr, packet->tag);
	pack_uint16(&curr, packet->length);
	pack_uint16(&curr, packet->opcode);
	pack_byte(&curr, packet->version);

	switch (packet->opcode) {
	case SMSG_HANDSHAKE:
	case CMSG_HANDSHAKE:
		pack_int32(&curr, packet->handshake.id);
		break;
	case CMSG_LAUNCH_PROCESS:
		if (!packet->cmsg_launch_process.name) {
			free(packed_data);
			return NULL;
		}
		pack_pascal_string(&curr, packet->cmsg_launch_process.name);
		break;
	case SMSG_APP_STARTED:
		pack_int32(&curr, packet->smsg_app_started.pid);
		break;
	case SMSG_APP_TERMINATED:
		pack_int32(&curr, packet->smsg_app_terminated.pid);
		break;
	case SMSG_APP_TERMINATED_BY_OTHER:
		pack_int32(&curr, packet->smsg_app_terminated_by_other.pid);
		break;
	case SMSG_APP_ALIVE:
		pack_int32(&curr, packet->smsg_app_alive.pid);
		break;
	case CMSG_TERMINATE_PROCESS:
		pack_int32(&curr, packet->cmsg_terminate_process.pid);
		break;
	default:
		break;
	}

	return packed_data;
}

int packet_unpack(const byte *data, uint len, Packet **packet) {
	if (!data || !packet)
		return -1;

	if (*packet != NULL) {
		packet_free(*packet);
		*packet = NULL;
	}

	if (len < PACKET_HEADER_SIZE)
		return 0;

	uint32 tag = unpack_uint32(&data);
	uint16 length = unpack_uint16(&data);

	if (tag != PACKET_TAG)
		return -1;

	if (length < len)
		return 0;

	Packet *pck = packet_create();
	if (!pck)
		return -1;

	pck->length = length;
	pck->opcode = unpack_uint16(&data);
	pck->version = unpack_byte(&data);

	if (pck->version != PROTOCOL_VERSION || pck->opcode >= MSG_INVALID) {
		packet_free(pck);
		return -1;
	}

	switch (pck->opcode) {
	case SMSG_HANDSHAKE:
	case CMSG_HANDSHAKE:
		pck->handshake.id = unpack_int32(&data);
		break;
	case CMSG_LAUNCH_PROCESS:
		pck->cmsg_launch_process.name = unpack_pascal_string(&data, &pck->cmsg_launch_process.name_len);
		break;
	case SMSG_APP_STARTED:
		pck->smsg_app_started.pid = unpack_int32(&data);
		break;
	case CMSG_TERMINATE_PROCESS:
		pck->cmsg_terminate_process.pid = unpack_int32(&data);
		break;
	case SMSG_APP_TERMINATED:
		pck->smsg_app_terminated.pid = unpack_int32(&data);
		break;
	case SMSG_APP_TERMINATED_BY_OTHER:
		pck->smsg_app_terminated_by_other.pid = unpack_int32(&data);
		break;
	case SMSG_APP_ALIVE:
		pck->smsg_app_alive.pid = unpack_int32(&data);
		break;
	default:
		break;
	}

	if (pck->opcode == CMSG_LAUNCH_PROCESS && !pck->cmsg_launch_process.name) {
		packet_free(pck);
		return -1;
	}

	if (packet_calculate_length(pck) != pck->length) {
		packet_free(pck);
		return -1;
	}

	*packet = pck;
	return pck->length;
}
