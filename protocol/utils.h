#ifndef UTILS_H
#define UTILS_H

#include <assert.h>
#include <malloc.h>
#include <string.h>

#include "types.h"

#define MKTAG(a0, a1, a2, a3) ((int)((a3) | ((a2) << 8) | ((a1) << 16) | ((a0) << 24)))

inline void pack_byte(byte **buffer, byte v) {
	**buffer = v;
	*buffer += 1;
}

inline void pack_int16(byte **buffer, int16 v) {
	pack_byte(buffer, v >> 16);
	pack_byte(buffer, (byte)v);
}

inline void pack_int32(byte **buffer, int32 v) {
	pack_byte(buffer, v >> 24);
	pack_byte(buffer, v >> 16);
	pack_byte(buffer, v >> 8);
	pack_byte(buffer, v);
}

inline void pack_uint16(byte **buffer, uint16 v) {
	pack_int16(buffer, v);
}

inline void pack_uint32(byte **buffer, uint32 v) {
	pack_int32(buffer, v);
}

inline void pack_pascal_string(byte **buffer, const char *s) {
	assert(s);
	byte len = (byte)strlen(s);
	pack_byte(buffer, len);
	memcpy(*buffer, s, len);
	*buffer += len;
}

inline byte unpack_byte(const byte **buffer) {
	byte b = **buffer;
	*buffer += 1;
	return b;
}

inline int16 unpack_int16(const byte **buffer) {
	int16 v = ((*buffer)[0] << 8) | (*buffer)[1];
	*buffer += 2;
	return v;
}

inline int32 unpack_int32(const byte **buffer) {
	int32 v = (((*buffer)[0] << 24) | ((*buffer)[1] << 16) | ((*buffer)[2] << 8) | (*buffer)[3]);
	*buffer += 4;
	return v;
}

inline uint16 unpack_uint16(const byte **buffer) {
	return unpack_int16(buffer);
}

inline uint32 unpack_uint32(const byte **buffer) {
	return unpack_int32(buffer);
}

inline char *unpack_pascal_string(const byte **buffer, byte *len) {
	byte length = unpack_byte(buffer);
	char *s = (char *)malloc(length + 1);
	if (!s) {
		if (len)
			*len = 0;
		return NULL;
	}

	memcpy(s, *buffer, length);
	s[length] = '\0';

	if (len) {
		*len = length;
	}

	return s;
}

#endif /* UTILS_H */
