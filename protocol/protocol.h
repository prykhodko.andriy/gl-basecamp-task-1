#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "types.h"

#define PROTOCOL_VERSION 1
#define PACKET_HEADER_SIZE 13
#define PACKET_MAX_SIZE (PACKET_HEADER_SIZE + 256 + 1)

#define HANDSHAKE_TIMEOUT 1000

/**
 * @brief Opcodes
 * 
 * CMSG messages should be processed by client
 * SMSG messages should be processed by server
 */

enum Opcodes {
	MSG_NULL,

	CMSG_HANDSHAKE,
	SMSG_HANDSHAKE,

	CMSG_LAUNCH_PROCESS,
	SMSG_APP_STARTED,
	SMSG_APP_START_ERROR,
	CMSG_TERMINATE_PROCESS,
	SMSG_APP_TERMINATED,
	SMSG_APP_TERMINATED_BY_OTHER,
	SMSG_APP_ALIVE,

	MSG_INVALID
};

/**
 * @brief Packet layout
 * 
 * To allocate packet use packet_create function
 * To free packet use packet_free function
 * 
 * To send and read structure in machine-independent form
 * use functions packet_pack and packet_unpack
 * 
 * @note sizeof(Packet) must not be used outside of protocol module
 */
typedef struct Packet {
	uint32 tag;
	uint16 length;
	uint16 opcode;
	byte version;

	union {
		struct {
			int32 id;
		} handshake;

		struct {
			byte name_len;
			char *name;
		} cmsg_launch_process;

		struct {
			int32 pid;
		} smsg_app_started;

		struct {
			int32 pid;
		} cmsg_terminate_process;

		struct {
			int32 pid;
		} smsg_app_terminated;

		struct {
			int32 pid;
		} smsg_app_terminated_by_other;

		struct {
			int32 pid;
		} smsg_app_alive;
	};
} Packet;

/**
 * @brief Create the packet
 * 
 * @return Newly created packet filled with default values or NULL on failure
 */
Packet *packet_create();

/**
 * @brief Free the packet
 * 
 * @param packet Packet to be freed
 * @note Packet pointers to strings will be freed
 */
void packet_free(Packet *packet);

/**
 * @brief Pack the packet
 * 
 * @param packet packet to be packed
 * 
 * @return Pointer to packed data on success, NULL on error.
 *         Pointer must be freed by free()
 * @note Packet length is calculated by this function.
 * 		 Packet strings lengthes are calculated by this function
 */
byte *packet_pack(Packet *packet);

/**
 * @brief Unpack the packet
 * 
 * @param data data to be unpacked
 * @param len data length
 * @param packet unpacked packet will be stored here
 * 
 * @return packet length on success, 0 on insufficient length, -1 on error 
 * @note if user supplied not NULL pointer to packet it will be freed 
 */
int packet_unpack(const byte *data, uint len, Packet **packet);

#endif /* PROTOCOL_H */
