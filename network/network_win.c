#include "network.h"

#define WIN32_LEAN_AND_MEAN

#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "ws2_32.lib")

bool socket_global_init() {
	WSADATA data = {0};
	return WSAStartup(MAKEWORD(2, 2), &data) == 0;
}

bool socket_global_deinit() {
	return WSACleanup() == 0;
}

Socket socket_create_tcp() {
	return (Socket)socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

void socket_close(Socket socket) {
	closesocket(socket);
}

bool socket_connect(Socket socket, const char *ip, uint16 port) {
	struct sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_port = htons(port);

	if (-1 == inet_pton(AF_INET, ip, &service.sin_addr))
		return false;

	return connect(socket, (SOCKADDR *)&service, sizeof(service)) == 0;
}

int socket_get_last_error() {
	return WSAGetLastError();
}

bool socket_is_blocking_error() {
	return WSAGetLastError() == WSAEWOULDBLOCK;
}

bool socket_set_blocking(Socket socket, bool v) {
	u_long mode = !v;
	return ioctlsocket(socket, FIONBIO, &mode) == 0;
}

bool socket_bind(Socket socket, const char *ip, uint16 port) {
	struct sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_port = htons(port);

	if (-1 == inet_pton(AF_INET, ip, &service.sin_addr))
		return false;
	return bind(socket, (SOCKADDR *)&service, sizeof(service)) == 0;
}

bool socket_listen(Socket socket) {
	return listen(socket, SOMAXCONN) == 0;
}

Socket socket_accept(Socket socket) {
	return (Socket)accept(socket, NULL, NULL);
}

int socket_read(Socket sck, char *buffer, uint length) {
	return recv(sck, buffer, length, 0);
}

int socket_write(Socket sck, const char *buffer, uint length) {
	return send(sck, buffer, length, 0);
}
