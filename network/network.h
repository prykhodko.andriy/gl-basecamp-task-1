#ifndef NETWORK_H
#define NETWORK_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef unsigned int uint;
typedef uint16_t uint16;
typedef int Socket;

#define INVALID_SOCKET (-1)

bool socket_global_init();

bool socket_global_deinit();

Socket socket_create_tcp();

void socket_close(Socket socket);

bool socket_connect(Socket socket, const char *ip, uint16 port);

int socket_get_last_error();

bool socket_is_blocking_error();

bool socket_set_blocking(Socket socket, bool v);

bool socket_bind(Socket socket, const char *ip, uint16 port);

bool socket_listen(Socket socket);

Socket socket_accept(Socket socket);

int socket_read(Socket sck, char *buffer, uint length);

int socket_write(Socket sck, const char *buffer, uint length);

#endif /* NETWORK_H */
