#include "network.h"

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

bool socket_global_init() {
	return true;
}

bool socket_global_deinit() {
	return true;
}

Socket socket_create_tcp() {
	return (Socket)socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

void socket_close(Socket socket) {
	close(socket);
}

bool socket_connect(Socket socket, const char *ip, uint16 port) {
	struct sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_port = htons(port);

	if (-1 == inet_pton(AF_INET, ip, &service.sin_addr))
		return false;

	return connect(socket, (struct sockaddr *)&service, sizeof(service)) == 0;
}

int socket_get_last_error() {
	return errno;
}

bool socket_is_blocking_error() {
	return (errno == EWOULDBLOCK || errno == EAGAIN);
}

bool socket_set_blocking(Socket socket, bool v) {
	int flags = fcntl(socket, F_GETFL);
	if (v) {
		flags |= O_NONBLOCK;
	} else {
		flags &= ~O_NONBLOCK;
	}
	return fcntl(socket, F_SETFL, flags) == 0;
}

bool socket_bind(Socket socket, const char *ip, uint16 port) {
	struct sockaddr_in service;
	service.sin_family = AF_INET;
	service.sin_port = htons(port);

	if (-1 == inet_pton(AF_INET, ip, &service.sin_addr))
		return false;
	return bind(socket, (struct sockaddr *)&service, sizeof(service)) == 0;
}

bool socket_listen(Socket socket) {
	return listen(socket, SOMAXCONN) == 0;
}

Socket socket_accept(Socket socket) {
	return (Socket)accept(socket, NULL, NULL);
}

int socket_read(Socket sck, char *buffer, uint length) {
	return recv(sck, buffer, length, 0);
}

int socket_write(Socket sck, const char *buffer, uint length) {
	return send(sck, buffer, length, 0);
}
