cmake_minimum_required(VERSION 3.0.0)
project(libterminal)

set(CMAKE_C_STANDARD 11)

if(WIN32)
    set(SOURCES terminal_win.c)
elseif(UNIX)
    error(FATAL_ERROR "terminal module is not implemented for posix.")
    #set(SOURCES terminal_posix.c)
endif()

add_library(terminal STATIC ${SOURCES})
