#include <Windows.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

#include "terminal.h"

static HANDLE hConsole = INVALID_HANDLE_VALUE;

bool terminal_init() {
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	// Get the number of character cells in the current buffer.

	if (!GetConsoleScreenBufferInfo(hConsole, &csbi)) {
		return false;
	}

	return true;
}

bool terminal_deinit() {
	return true;
}

void terminal_clear_line(uint line) {
	COORD coordScreen = {0, line}; // home for the cursor
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;

	fflush(stdout);

	// Get the number of character cells in the current buffer.

	if (!GetConsoleScreenBufferInfo(hConsole, &csbi)) {
		return;
	}

	dwConSize = csbi.dwSize.X;

	// Fill the entire screen with blanks.

	if (!FillConsoleOutputCharacter(hConsole,        // Handle to console screen buffer
	                                (TCHAR)' ',      // Character to write to the buffer
	                                dwConSize,       // Number of cells to write
	                                coordScreen,     // Coordinates of first cell
	                                &cCharsWritten)) // Receive number of characters written
	{
		return;
	}

	// Get the current text attribute.

	if (!GetConsoleScreenBufferInfo(hConsole, &csbi)) {
		return;
	}

	// Set the buffer's attributes accordingly.

	if (!FillConsoleOutputAttribute(hConsole,         // Handle to console screen buffer
	                                csbi.wAttributes, // Character attributes to use
	                                dwConSize,        // Number of cells to set attribute
	                                coordScreen,      // Coordinates of first cell
	                                &cCharsWritten))  // Receive number of characters written
	{
		return;
	}

	// Put the cursor at its home coordinates.

	SetConsoleCursorPosition(hConsole, coordScreen);
}

void terminal_set_cursor_pos(uint x, uint y) {
	fflush(stdout);
	COORD coord = {x, y};
	SetConsoleCursorPosition(hConsole, coord);
}

bool terminal_kbhit() {
	return _kbhit();
}

int terminal_getch() {
	return _getch();
}

void terminal_printf(const char *fmt, ...) {
	va_list args;

	va_start(args, fmt);
	int res = vprintf(fmt, args);
	va_end(args);
}

void terminal_putchar(char c) {
	putchar(c);
}

bool terminal_flush() {
	return fflush(stdout);
}
