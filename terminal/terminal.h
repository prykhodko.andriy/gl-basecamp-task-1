#ifndef TERMINAL_H
#define TERMINAL_H

#include <stdbool.h>

typedef unsigned int uint;

bool terminal_init();

bool terminal_deinit();

void terminal_clear_line(uint line);

void terminal_set_cursor_pos(uint x, uint y);

bool terminal_kbhit();

int terminal_getch();

void terminal_printf(const char *fmt, ...);

void terminal_putchar(char c);

bool terminal_flush();

#endif /* TERMINAL_H */
