#include "app_server.h"

#include <assert.h>
#include <ctype.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "network/network.h"
#include "platform/platform.h"
#include "protocol/protocol.h"
#include "terminal/terminal.h"

#define ARRAYSIZE(ar) (sizeof(ar) / sizeof(ar[0]))

#define APP_LOG_SIZE 16

#define BACKSPACE '\b'
#define ENTER '\r'
#define CTRL_D '\x4'

typedef struct Connection {
	Socket socket;
	int id;
	int handshake_passed;
	int time;
	byte buffer[PACKET_MAX_SIZE * 2];
	int buffer_bytes;
} Connection;

enum State {
	STATE_LISTENING,
	STATE_CONNECTED,
	STATE_PROCESS_RUNNING,
};

typedef struct Server {
	bool init;

	Socket server;
	int state;

	Connection conn;
	int should_exit;
	int exit_code;

	bool redraw;
	char input[256];
	char log[APP_LOG_SIZE][256];
	int logIndex;
} Server;

static Server g_app;

static void server_log(const char *fmt, ...) {
	if (!fmt)
		return;

	va_list args;

	va_start(args, fmt);
	vsprintf(g_app.log[g_app.logIndex], fmt, args);
	va_end(args);

	g_app.logIndex = (g_app.logIndex + 1) % ARRAYSIZE(g_app.log);
	g_app.redraw = true;
}

static void server_disconnect_client() {
	socket_close(g_app.conn.socket);
	g_app.conn.socket = INVALID_SOCKET;
	server_log("Disconnect client");
}

static void server_handle_packet(Packet *pck) {
	if (!pck)
		return;

	if (!g_app.conn.handshake_passed && pck->opcode != SMSG_HANDSHAKE) {
		free(pck);
		return;
	}

	switch (pck->opcode) {
	case SMSG_HANDSHAKE:
		g_app.conn.handshake_passed = (pck->handshake.id == g_app.conn.id);
		break;
	case SMSG_APP_STARTED:
		g_app.state = STATE_PROCESS_RUNNING;
		server_log("App started %d", pck->smsg_app_started.pid);
		break;
	case SMSG_APP_START_ERROR:
		server_log("App start error");
		break;
	case SMSG_APP_ALIVE:
		server_log("App alive");
		break;
	case SMSG_APP_TERMINATED:
		g_app.state = STATE_CONNECTED;
		server_log("App terminated");
		break;
	case SMSG_APP_TERMINATED_BY_OTHER:
		g_app.state = STATE_CONNECTED;
		server_log("App terminated by other");
		break;
	default:
		server_log("Unknown opcode");
		break;
	}

	free(pck);
}

static void server_update_connection() {
	if (g_app.state == STATE_CONNECTED && !g_app.conn.handshake_passed &&
	    (platform_get_millis() - g_app.conn.time) > HANDSHAKE_TIMEOUT) {

		server_log("Handshake failed");
		g_app.state = STATE_LISTENING;

		server_disconnect_client();
		return;
	}

	int res = socket_read(g_app.conn.socket, (char *)g_app.conn.buffer, sizeof(g_app.conn.buffer) - g_app.conn.buffer_bytes);
	if (res == 0) {
		server_disconnect_client();
		return;
	}

	Packet *packet = NULL;
	while (true) {
		int res = packet_unpack(g_app.conn.buffer, g_app.conn.buffer_bytes, &packet);
		if (res > 0) {
			assert(packet);
			memmove(g_app.conn.buffer, g_app.conn.buffer + packet->length, g_app.conn.buffer_bytes - packet->length);
			g_app.conn.buffer_bytes -= packet->length;

			server_handle_packet(packet);
		} else if (res < 0) {
			server_disconnect_client();
			break;
		} else {
			// not enough data
			break;
		}
	}
}

static void server_exit(int code) {
	g_app.exit_code = code;
	g_app.should_exit = true;
}

static bool server_send_packet(Packet *packet) {
	assert(packet);

	byte *data = packet_pack(packet);
	if (!data)
		return false;

	int res = socket_write(g_app.conn.socket, (char *)data, packet->length);
	if (res < 0 || res != packet->length) {
		free(data);
		return false;
	}

	free(data);
	return true;
}

static void server_accept_connection() {
	Socket sck = socket_accept(g_app.server);
	if (sck == INVALID_SOCKET) {
		if (!socket_is_blocking_error())
			server_exit(socket_get_last_error());
		return;
	}

	if (g_app.conn.socket != INVALID_SOCKET) {
		socket_close(sck);
		return;
	}

	server_log("New Connection");
	g_app.conn.socket = sck;
	g_app.conn.id = rand();
	g_app.conn.time = platform_get_millis();
	g_app.conn.buffer_bytes = 0;
	g_app.conn.handshake_passed = false;

	Packet *packet = packet_create();
	assert(packet);
	packet->opcode = CMSG_HANDSHAKE;
	packet->handshake.id = g_app.conn.id;

	if (!server_send_packet(packet)) {
		server_disconnect_client();
	}

	packet_free(packet);
}

static void server_clear_input() {
	g_app.redraw = true;
	g_app.input[0] = '\0';
}

static void server_process_input() {
	char *token = strtok(g_app.input, " ");
	if (!token || strcmp(token, "launch")) {
		server_clear_input();
		return;
	}

	token = strtok(NULL, " ");
	if (token && STATE_CONNECTED == g_app.state) {
		Packet *pck = packet_create();
		pck->opcode = CMSG_LAUNCH_PROCESS;
		pck->cmsg_launch_process.name = strdup(token);

		server_send_packet(pck);
		free(pck);
	}

	server_clear_input();
}

void server_draw() {
	if (!g_app.redraw)
		return;

	terminal_clear_line(0);
	terminal_printf(">%s", g_app.input);

	terminal_clear_line(1);
	switch (g_app.state) {
	case STATE_CONNECTED:
		terminal_printf("STATUS: CONNECTED\n");
		break;
	case STATE_LISTENING:
		terminal_printf("STATUS: LISTENING\n");
		break;
	case STATE_PROCESS_RUNNING:
		terminal_printf("STATUS: PROCESS_RUNNING\n");
		break;
	default:
		break;
	}

	terminal_printf("Log:\n");
	for (int i = 0; i < ARRAYSIZE(g_app.log); ++i) {
		terminal_clear_line(i + 2);
		terminal_printf("%c%s\n", (g_app.logIndex == i) ? '$' : ' ', g_app.log[i]);
	}
	terminal_set_cursor_pos(strlen(g_app.input) + 1, 0);
	terminal_flush();

	g_app.redraw = false;
}

static void server_update_input() {
	while (terminal_kbhit()) {
		int ch = terminal_getch();
		switch (ch) {
		case BACKSPACE:
			int len = (int)strlen(g_app.input);
			if (len > 0) {
				g_app.input[len - 1] = '\0';
				g_app.redraw = true;
			}
			break;
		case ENTER:
			server_process_input();
			break;
		case CTRL_D:
			server_exit(0);
			return;
		default:
			if (isprint(ch)) {
				int len = (int)strlen(g_app.input);
				if (len < sizeof(g_app.input) - 1) {
					g_app.input[len] = ch;
					g_app.input[len + 1] = '\0';
					g_app.redraw = true;
				}
			}
			break;
		}
	}
}

int server_init(int argc, char *argv[]) {
	if (g_app.init)
		return 0;

	if (!terminal_init())
		return -1;

	if (!socket_global_init())
		return -1;

	g_app.conn.socket = INVALID_SOCKET;
	g_app.server = socket_create_tcp();
	g_app.state = STATE_LISTENING;
	if (g_app.server == INVALID_SOCKET)
		return socket_get_last_error();

	if (!socket_bind(g_app.server, "0.0.0.0", 7654) || !socket_set_blocking(g_app.server, false)) {
		socket_close(g_app.server);
		return socket_get_last_error();
	}

	g_app.init = true;
	return 0;
}

int server_run() {
	if (!g_app.init)
		return -1;

	if (!socket_listen(g_app.server))
		return socket_get_last_error();

	g_app.should_exit = false;
	g_app.redraw = true;

	while (!g_app.should_exit) {
		server_accept_connection();
		server_update_connection();
		server_update_input();
		server_draw();

		platform_sleep(10);
	}

	return g_app.exit_code;
}
