#ifndef PLATFORM_H
#define PLATFORM_H

#include <stdbool.h>

typedef unsigned int uint;

/**
 * @brief 
 * Sleep for the specified amount of milliseconds
 */

void platform_sleep(uint ms);

/**
 * @brief 
 * Get the number of milliseconds since the system was started
 */

uint platform_get_millis();

#endif /* PLATFORM_H */
