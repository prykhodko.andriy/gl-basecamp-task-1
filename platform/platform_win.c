#include <Windows.h>

#include "platform.h"

void platform_sleep(uint ms) {
	Sleep(ms);
}

uint platform_get_millis() {
	return GetTickCount();
}
