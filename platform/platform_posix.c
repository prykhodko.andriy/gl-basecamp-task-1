#include <time.h>
#include <unistd.h>

#include "platform.h"

void platform_sleep(uint ms) {
	usleep(ms * 1000);
}

uint platform_get_millis() {
	struct timespec res;
	clock_gettime(CLOCK_MONOTONIC, &res);
	return res.tv_sec * 1000 + res.tv_nsec / 1000000;
}
