#include <stdio.h>

#ifdef CLIENT
#include "client.h"
#else
#include "app_server.h"
#endif

int main(int argc, char *argv[]) {
	int error_code = server_init(argc, argv);
	if (error_code != 0)
		return error_code;
	return server_run();
}
