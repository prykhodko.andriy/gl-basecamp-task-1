#ifndef APP_SERVER_H
#define APP_SERVER_H

int server_init(int argc, char *argv[]);

int server_run();

#endif /* APP_SERVER_H */
